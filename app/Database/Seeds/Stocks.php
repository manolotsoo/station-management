<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Stocks extends Seeder
{
  protected $tableName = 'stocks';

  public function run()
  {
    $data = [
      [
        'product_id' => 1,
        'quantity_entry' => 345,
      ],[
        'product_id' => 2,
        'quantity_entry' => 452,
      ],[
        'product_id' => 3,
        'quantity_entry' => 128,
      ],
    ];
    // Using Query Builder
    $this->db->table($this->tableName)->insertBatch($data);
  }
}
