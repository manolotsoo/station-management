<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Sales extends Seeder
{
  protected $tableName = 'sales';

  public function run()
  {
    $data = [
      [
        'product_id' => 1,
        // 'responsable' => 1,
        'quantity' => 12,
        'total_price' => 24000,
      ],
      [
        'product_id' => 2,
        // 'responsable' => 1,
        'quantity' => 14,
        'total_price' => 42000,
      ],
      [
        'product_id' => 3,
        // 'responsable' => 1,
        'quantity' => 5,
        'total_price' => 17000,
      ],
    ];
    // Using Query Builder
    $this->db->table($this->tableName)->insertBatch($data);
  }
}
