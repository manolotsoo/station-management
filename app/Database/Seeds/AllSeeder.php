<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class AllSeeder extends Seeder
{
  public function run()
  {
    $this->call('Users');
    // $this->call('Products');
    $this->call('Stocks');
    // $this->call('Sales');
  }
}
