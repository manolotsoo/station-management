<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Users extends Seeder
{
  protected $tableName = 'users';

  public function run()
  {
    $data = [
      [
        'email' => 'manolotsoa@station1.com',
        'user_name' => 'manolotsoa',
        '_password' => password_hash('123456', PASSWORD_BCRYPT)
      ],
      [
        'email' => 'razafindrakoto@station1.com',
        'user_name' => 'razafindrakoto',
        '_password' => password_hash('123456', PASSWORD_BCRYPT)
      ],
    ];
    // Using Query Builder
    $this->db->table($this->tableName)->insertBatch($data);
  }
}
