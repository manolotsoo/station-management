<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Stocks extends Migration
{
  protected $tableName = 'stocks';

	public function up()
	{
		$this->forge->addField(array(
      'id' => array(
        'type' => 'INT',
        'constraint' => 4,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'product_id' => [
        'type' => 'INT',
        'constraint' => 4,
        'unsigned' => TRUE
      ],
      'quantity_entry' => [
        'type' => 'DOUBLE',
        'unsigned' => TRUE,
      ],
      'quantity_out' => [
        'type' => 'DOUBLE',
        'unsigned' => TRUE,
      ]
    ));
    $this->forge->addKey('id', TRUE);
    $this->forge->addField("updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP");
    $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
    $this->forge->addField('CONSTRAINT FOREIGN KEY (product_id) REFERENCES products(id) ON DELETE RESTRICT ON UPDATE CASCADE');
    $this->forge->createTable($this->tableName);
	}

	public function down()
	{
    $this->forge->dropTable($this->tableName);
	}
}
