<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Users extends Migration
{
  protected $tableName = 'users';

  public function up()
  {
    $this->forge->addField(array(
      'id' => array(
        'type' => 'INT',
        'constraint' => 4,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'email' => [
        'type' => 'VARCHAR',
        'constraint' => '100',
        'unique' => TRUE,
      ],
      'user_name' => [
        'type' => 'VARCHAR',
        'constraint' => '50',
        'unique' => TRUE,
      ],
      '_password' => [
        'type' => 'VARCHAR',
        'constraint' => '100',
      ]
    ));
    $this->forge->addKey('id', TRUE);
    $this->forge->addField("updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP");
    $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
    $this->forge->createTable($this->tableName);

  }

  public function down()
  {
    $this->forge->dropTable($this->tableName);
  }
}
