<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Products extends Migration
{
  protected $tableName = 'products';

  public function up()
  {
    $this->forge->addField(array(
      'id' => array(
        'type' => 'INT',
        'constraint' => 4,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      '_name' => [
        'type' => 'VARCHAR',
        'constraint' => '100'
      ],
      // 'responsable' => [
      //   'type' => 'INT',
      //   'constraint' => 4,
      //   'unsigned' => TRUE
      // ],
      'cost_price' => [
        'type' => 'DOUBLE',
        'unsigned' => TRUE
      ],
      'selling_price' => [
        'type' => 'DOUBLE',
        'unsigned' => TRUE
      ],
      'evaporation_percent' => [
        'type' => 'DOUBLE',
        'unsigned' => TRUE
      ]
    ));
    $this->forge->addKey('id', TRUE);
    $this->forge->addField("updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP");
    $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
    // $this->forge->addField('CONSTRAINT FOREIGN KEY (responsable) REFERENCES users(id) ON DELETE RESTRICT ON UPDATE CASCADE');
    $this->forge->createTable($this->tableName);
  }

  public function down()
  {
    $this->forge->dropTable($this->tableName);
  }
}
