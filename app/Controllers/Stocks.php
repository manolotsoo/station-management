<?php
// ADEL CODEIGNITER 4 CRUD GENERATOR

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Models\StockModel;

class Stocks extends BaseController
{

  protected $stocksModel;
  protected $validation;

  public function __construct()
  {
    $this->stocksModel = new StockModel();
    $this->validation =  \Config\Services::validation();
    helper(['users', 'products', 'stocks']);
  }

  public function index()
  {

    $data = [
      'controller'      => 'stocks',
      'title'         => 'Stocks',
      'titleLocation' => "Station 1",
      'titlePage' => "Mouvement de Stocks",
      'breadCrumb' => ["Home", "Stock"],
      'products' => allProduct()
    ];
    $data['state_stocks'] = get_state_stocks();
    return view('stocks', $data);
  }

  public function getAll()
  {
    $response = array();

    $data['data'] = array();

    $result = $this->stocksModel->select('id, product_id, quantity_entry, quantity_out, updated_at, created_at')
      // ->join('products','stocks.product_id=products.id')
      ->findAll();

    foreach ($result as $key => $value) {

      $ops = '<div class="btn-group">';
      $ops .= '	<button type="button" class="btn btn-sm btn-info" onclick="edit(' . $value->id . ')"><i class="fa fa-edit"></i></button>';
      $ops .= '	<button type="button" class="btn btn-sm btn-danger" onclick="remove(' . $value->id . ')"><i class="fa fa-trash"></i></button>';
      $ops .= '</div>';

      $data['data'][$key] = array(
        $value->id,
        $value->product_id,
        $value->quantity_entry,
        $value->quantity_out,
        $value->updated_at,
        $value->created_at,

        $ops,
      );
    }

    return $this->response->setJSON($data);
  }

  public function getOne()
  {
    $response = array();

    $id = $this->request->getPost('id');

    if ($this->validation->check($id, 'required|numeric')) {

      $data = $this->stocksModel->where('id', $id)->first();

      return $this->response->setJSON($data);
    } else {

      throw new \CodeIgniter\Exceptions\PageNotFoundException();
    }
  }

  public function add()
  {

    $response = array();

    $fields['id'] = $this->request->getPost('id');
    $fields['product_id'] = $this->request->getPost('productId');
    $fields['quantity_entry'] = $this->request->getPost('quantityEntry');
    $fields['quantity_out'] = $this->request->getPost('quantityOut');
    // $fields['updated_at'] = $this->request->getPost('updatedAt');
    // $fields['created_at'] = $this->request->getPost('createdAt');


    $this->validation->setRules([
      'product_id' => ['label' => 'Product id', 'rules' => 'required|numeric'],
      'quantity_entry' => ['label' => 'Quantity entry', 'rules' => 'permit_empty'],
      'quantity_out' => ['label' => 'Quantity out', 'rules' => 'permit_empty'],
      // 'updated_at' => ['label' => 'Updated at', 'rules' => 'permit_empty'],
      // 'created_at' => ['label' => 'Created at', 'rules' => 'permit_empty'],

    ]);

    if ($this->validation->run($fields) == FALSE) {

      $response['success'] = false;
      $response['messages'] = $this->validation->listErrors();
    } else {

      if ($this->stocksModel->insert($fields)) {

        $response['success'] = true;
        $response['messages'] = 'Data has been inserted successfully';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Insertion error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function edit()
  {

    $response = array();

    $fields['id'] = $this->request->getPost('id');
    $fields['product_id'] = $this->request->getPost('productId');
    $fields['quantity_entry'] = $this->request->getPost('quantityEntry');
    $fields['quantity_out'] = $this->request->getPost('quantityOut');
    // $fields['updated_at'] = $this->request->getPost('updatedAt');
    // $fields['created_at'] = $this->request->getPost('createdAt');


    $this->validation->setRules([
      'product_id' => ['label' => 'Product id', 'rules' => 'required|numeric'],
      'quantity_entry' => ['label' => 'Quantity entry', 'rules' => 'permit_empty'],
      'quantity_out' => ['label' => 'Quantity out', 'rules' => 'permit_empty'],
      // 'updated_at' => ['label' => 'Updated at', 'rules' => 'permit_empty'],
      // 'created_at' => ['label' => 'Created at', 'rules' => 'permit_empty'],

    ]);

    if ($this->validation->run($fields) == FALSE) {

      $response['success'] = false;
      $response['messages'] = $this->validation->listErrors();
    } else {

      if ($this->stocksModel->update($fields['id'], $fields)) {

        $response['success'] = true;
        $response['messages'] = 'Successfully updated';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Update error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function remove()
  {
    $response = array();

    $id = $this->request->getPost('id');

    if (!$this->validation->check($id, 'required|numeric')) {

      throw new \CodeIgniter\Exceptions\PageNotFoundException();
    } else {

      if ($this->stocksModel->where('id', $id)->delete()) {

        $response['success'] = true;
        $response['messages'] = 'Deletion succeeded';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Deletion error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function export_csv()
  {
    header("Content-Type: text/csv; charset=utf-8");
    header("Content-disposition: attachment; filename=stocks-station-2.csv");

    $out = fopen('php://output', 'w');

    $stockList = get_state_stocks();
    foreach ($stockList as $stock){
      fputcsv($out, array(
        $stock['product_id'],
        $stock['stock_state'],
      ));
    }

    fclose($out);
  }
}
