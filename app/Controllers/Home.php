<?php

namespace App\Controllers;

class Home extends BaseController
{

  public function __construct()
  {
    helper(['users']);
  }


  public function index()
  {
    if (!getIdConnected()) {
      return redirect()->to(base_url(["sessions", "new"]));
    }
    $data["titleLocation"] = "Station 1";
    $data["titlePage"] = "Etat";
    $data["breadCrumb"] = ["Home",""];
    return view('layouts/index', $data);
  }
}
