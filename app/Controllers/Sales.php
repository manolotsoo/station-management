<?php
// ADEL CODEIGNITER 4 CRUD GENERATOR

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ProductModel;
use App\Models\SaleModel;
use App\Models\StockModel;
use \Config\Database;


class Sales extends BaseController
{

  protected $saleModel;
  protected $validation;

  public function __construct()
  {
    $this->saleModel = new SaleModel();
    $this->stockModel = new StockModel();
    $this->validation =  \Config\Services::validation();
    helper(['users', 'products', 'date', 'stocks']);
  }

  public function index()
  {
    $productModel = new ProductModel();

    $data = [
      'controller'      => 'sales',
      'title'         => 'Sales',
      'titleLocation' => "Station-1",
      'titlePage' => "ventes",
      'breadCrumb' => ["Home", "Vente"],
      'products' => allProduct()
    ];
    // print_r(get_state_stocks(1)[0]['stock_state']);
    return view('sales', $data);
  }

  public function getAll()
  {
    $response = array();

    $data['data'] = array();

    $result = $this->saleModel->select('id, product_id, quantity, total_price, updated_at, created_at')->findAll();

    foreach ($result as $key => $value) {

      $ops = '<div class="btn-group">';
      $ops .= '	<button type="button" class="btn btn-sm btn-info" onclick="edit(' . $value->id . ')"><i class="fa fa-edit"></i></button>';
      $ops .= '	<button type="button" class="btn btn-sm btn-danger" onclick="remove(' . $value->id . ')"><i class="fa fa-trash"></i></button>';
      $ops .= '</div>';

      $data['data'][$key] = array(
        $value->id,
        $value->product_id,
        // $value->responsable,
        $value->quantity,
        $value->total_price,
        $value->updated_at,
        $value->created_at,

        $ops,
      );
    }

    return $this->response->setJSON($data);
  }

  public function getOne()
  {
    $response = array();

    $id = $this->request->getPost('id');

    if ($this->validation->check($id, 'required|numeric')) {

      $data = $this->saleModel->where('id', $id)->first();

      return $this->response->setJSON($data);
    } else {
      throw new \CodeIgniter\Exceptions\PageNotFoundException();
    }
  }

  public function add()
  {

    $response = array();

    $fields['id'] = $this->request->getPost('id');
    $fields['product_id'] = $this->request->getPost('productId');
    $fields['responsable'] = getIdConnected();


    $stock['product_id'] = $this->request->getPost('productId');

    $sales_type = $this->request->getPost('sales_type');
    $product = new ProductModel();
    $current_product = $product->find($fields['product_id']);

    if ($sales_type > 0) {
      // si montant 
      $fields['total_price'] = $this->request->getPost('totalPrice');
      $fields['quantity'] = $fields['total_price'] / $current_product->selling_price;
      $stock['quantity_out'] = $fields['total_price'] / $current_product->selling_price;
    } else {
      // si quantity
      $stock['quantity_out'] = $this->request->getPost('quantity');
      $fields['quantity'] = $this->request->getPost('quantity');
      $fields['total_price'] = $fields['quantity'] * $current_product->selling_price;
    }
    // $fields['updated_at'] = $this->request->getPost('updatedAt');
    // $fields['created_at'] = $this->request->getPost('createdAt');


    $this->validation->setRules([
      'product_id' => ['label' => 'Product id', 'rules' => 'required|numeric'],
      // 'responsable' => ['label' => 'Responsable', 'rules' => 'required|numeric'],
      'quantity' => ['label' => 'Quantity', 'rules' => 'permit_empty'],
      'total_price' => ['label' => 'Total price', 'rules' => 'permit_empty'],
      // 'updated_at' => ['label' => 'Updated at', 'rules' => 'permit_empty'],
      // 'created_at' => ['label' => 'Created at', 'rules' => 'permit_empty'],
    ]);
    if ($this->validation->run($fields) == FALSE) {
      $response['success'] = false;
      $response['messages'] = $this->validation->listErrors();
    } else {
      $state = get_quantity_product($stock['product_id']);
      if ($state != 0 && ($state - $stock['quantity_out']) < 0) {
        $stock['quantity_out'] = $state;
        $fields['quantity'] = $stock['quantity_out'];
        $fields['total_price'] = $stock['quantity_out'] * $current_product->selling_price;
      } else if ($state - $stock['quantity_out'] > 0) {
        $fields['quantity'] = $stock['quantity_out'];
        $fields['total_price'] = $stock['quantity_out'] * $current_product->selling_price;
        // print_r($fields);
      } else if ($fields['quantity'] > $state * $current_product->selling_price) {
        $fields['quantity'] = $state;
        $fields['total_price'] = $state * $current_product->selling_price;
        $stock['quantity_out'] = $fields['quantity'];
        $stock['quantity_out'] = $fields['quantity'];
      }
      if ($this->saleModel->insert($fields)) {
        if ($this->stockModel->insert($stock)) {
          return redirect()->to(base_url(["sales", "index"]));
        } else {
          echo "error insertion stock";
        }
      } else {
        echo "error insertion vente";
      }
    }
    return $this->response->setJSON($response);
  }

  public function edit()
  {
    $response = array();

    $fields['id'] = $this->request->getPost('id');
    $fields['product_id'] = $this->request->getPost('productId');
    // $fields['responsable'] = $this->request->getPost('responsable');
    $fields['quantity'] = $this->request->getPost('quantity');
    $fields['total_price'] = $this->request->getPost('totalPrice');
    // $fields['updated_at'] = $this->request->getPost('updatedAt');
    // $fields['created_at'] = $this->request->getPost('createdAt');


    $this->validation->setRules([
      'product_id' => ['label' => 'Product id', 'rules' => 'required|numeric'],
      // 'responsable' => ['label' => 'Responsable', 'rules' => 'required|numeric'],
      'quantity' => ['label' => 'Quantity', 'rules' => 'required'],
      'total_price' => ['label' => 'Total price', 'rules' => 'required'],
      // 'updated_at' => ['label' => 'Updated at', 'rules' => 'permit_empty'],
      // 'created_at' => ['label' => 'Created at', 'rules' => 'permit_empty'],

    ]);

    if ($this->validation->run($fields) == FALSE) {

      $response['success'] = false;
      $response['messages'] = $this->validation->listErrors();
    } else {

      if ($this->saleModel->update($fields['id'], $fields)) {

        $response['success'] = true;
        $response['messages'] = 'Successfully updated';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Update error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function remove()
  {
    $response = array();

    $id = $this->request->getPost('id');

    if (!$this->validation->check($id, 'required|numeric')) {

      throw new \CodeIgniter\Exceptions\PageNotFoundException();
    } else {

      if ($this->saleModel->where('id', $id)->delete()) {

        $response['success'] = true;
        $response['messages'] = 'Deletion succeeded';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Deletion error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function export_csv()
  {
    $location = $this->request->getPost('location');


    header("Content-Type: text/csv; charset=utf-8");
    // header("Content-disposition: attachment; filename=".$location."-".date('m-Y-d').".csv");
    header("Content-disposition: attachment; filename=Station-2-2021-07-29.csv");

    $out = fopen('php://output', 'w');
    $sales = new SaleModel();

    $saleList = $sales->where('DATE(created_at)', "2021-07-29")
      ->findAll();
    foreach ($saleList as $sale) {
      fputcsv($out, array(
        $sale->id,
        $sale->product_id,
        $sale->quantity,
        $sale->total_price,
      ));
    }

    fclose($out);
  }
}
