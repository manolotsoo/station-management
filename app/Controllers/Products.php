<?php
// ADEL CODEIGNITER 4 CRUD GENERATOR

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Models\ProductModel;
use App\Models\ImportationModel;

class Products extends BaseController
{

  protected $productModel;
  protected $validation;

  public function __construct()
  {
    $this->productModel = new ProductModel();
    $this->validation =  \Config\Services::validation();
    helper(['users']);
  }

  public function index()
  {
    $data = [
      'controller'      => 'products',
      'title'         => 'products',
      'titleLocation' => "Station 1",
      'titlePage' => "Produits",
      'breadCrumb' => ["Home", "Vente"],
      'breadCrumb' => ["Home", "Produit"]
    ];

    return view('products', $data);
  }

  public function getAll()
  {
    $response = array();

    $data['data'] = array();

    $result = $this->productModel->select('id, _name, cost_price, selling_price, updated_at, created_at')->findAll();

    foreach ($result as $key => $value) {

      $ops = '<div class="btn-group">';
      $ops .= '	<button type="button" class="btn btn-sm btn-info" onclick="edit(' . $value->id . ')"><i class="fa fa-edit"></i></button>';
      $ops .= '	<button type="button" class="btn btn-sm btn-danger" onclick="remove(' . $value->id . ')"><i class="fa fa-trash"></i></button>';
      $ops .= '</div>';

      $data['data'][$key] = array(
        $value->id,
        $value->_name,
        // $value->responsable,
        $value->cost_price,
        $value->selling_price,
        $value->evaporation_percent,
        $value->updated_at,
        $value->created_at,

        $ops,
      );
    }

    return $this->response->setJSON($data);
  }

  public function getOne()
  {
    $response = array();

    $id = $this->request->getPost('id');

    if ($this->validation->check($id, 'required|numeric')) {

      $data = $this->productModel->where('id', $id)->first();

      return $this->response->setJSON($data);
    } else {

      throw new \CodeIgniter\Exceptions\PageNotFoundException();
    }
  }

  public function add()
  {

    $response = array();

    $fields['id'] = $this->request->getPost('id');
    $fields['_name'] = $this->request->getPost('name');
    // $fields['responsable'] = $this->request->getPost('responsable');
    $fields['cost_price'] = $this->request->getPost('costPrice');
    $fields['selling_price'] = $this->request->getPost('sellingPrice');
    $fields['evaporation_percent'] = $this->request->getPost('evaporationPercent');
    // $fields['updated_at'] = $this->request->getPost('updatedAt');
    // $fields['created_at'] = $this->request->getPost('createdAt');


    $this->validation->setRules([
      '_name' => ['label' => ' name', 'rules' => 'required|max_length[100]'],
      // 'responsable' => ['label' => 'Responsable', 'rules' => 'required|numeric'],
      'cost_price' => ['label' => 'Cost price', 'rules' => 'required'],
      'selling_price' => ['label' => 'Selling price', 'rules' => 'required'],
      'evaporation_percent' => ['label' => 'Evaporation percent', 'rules' => 'required'],
      // 'updated_at' => ['label' => 'Updated at', 'rules' => 'permit_empty'],
      // 'created_at' => ['label' => 'Created at', 'rules' => 'permit_empty'],

    ]);

    if ($this->validation->run($fields) == FALSE) {

      $response['success'] = false;
      $response['messages'] = $this->validation->listErrors();
    } else {

      if ($this->productModel->insert($fields)) {

        $response['success'] = true;
        $response['messages'] = 'Data has been inserted successfully';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Insertion error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function edit()
  {

    $response = array();

    $fields['id'] = $this->request->getPost('id');
    $fields['_name'] = $this->request->getPost('name');
    $fields['responsable'] = $this->request->getPost('responsable');
    $fields['cost_price'] = $this->request->getPost('costPrice');
    $fields['selling_price'] = $this->request->getPost('sellingPrice');
    $fields['evaporation_percent'] = $this->request->getPost('evaporationPercent');
    $fields['updated_at'] = $this->request->getPost('updatedAt');
    $fields['created_at'] = $this->request->getPost('createdAt');


    $this->validation->setRules([
      '_name' => ['label' => ' name', 'rules' => 'required|max_length[100]'],
      'responsable' => ['label' => 'Responsable', 'rules' => 'required|numeric'],
      'cost_price' => ['label' => 'Cost price', 'rules' => 'required'],
      'selling_price' => ['label' => 'Selling price', 'rules' => 'required'],
      'evaporation_percent' => ['label' => 'Evaporation percent', 'rules' => 'required'],
      'updated_at' => ['label' => 'Updated at', 'rules' => 'permit_empty'],
      'created_at' => ['label' => 'Created at', 'rules' => 'permit_empty'],

    ]);

    if ($this->validation->run($fields) == FALSE) {

      $response['success'] = false;
      $response['messages'] = $this->validation->listErrors();
    } else {

      if ($this->productModel->update($fields['id'], $fields)) {

        $response['success'] = true;
        $response['messages'] = 'Successfully updated';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Update error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function remove()
  {
    $response = array();

    $id = $this->request->getPost('id');

    if (!$this->validation->check($id, 'required|numeric')) {

      throw new \CodeIgniter\Exceptions\PageNotFoundException();
    } else {

      if ($this->productModel->where('id', $id)->delete()) {

        $response['success'] = true;
        $response['messages'] = 'Deletion succeeded';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Deletion error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function import_csv()
  {
    $row = 1;
    $totalProducts = array();

    $file_to_upload_name = $_FILES['file_to_upload']['name'];
    $nameOfStation = explode("-", $file_to_upload_name);
    // $location = implode("-", [$nameOfStation[0], $nameOfStation[1]]);

    if (($handle = fopen($_FILES['file_to_upload']['tmp_name'], "r")) !== FALSE) {
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $row++;
        array_push($totalProducts, array(
          'id' => $data[0],
          '_name' => $data[1],
          'cost_price' => $data[2],
          'selling_price' => $data[3]
        ));
      }
      fclose($handle);
    }
    return View('products_importation_view', [
      'totalProducts' => $totalProducts,
      'controller'      => 'products',
      'title'         => 'Produit',
      // 'titleLocation' => $location,
      'filename' => $file_to_upload_name,
      'titlePage' => "produits",
      'breadCrumb' => ["Home", "produits", "aperçu"],
      'row_limit' => $row
    ]);
  }

  public function save()
  {
    $row_limit = $this->request->getPost('row_limit');
    $file_name = $this->request->getPost('file_name');
    $products = array();
    $this->validation->setRules([
      '_name' => ['label' => ' name', 'rules' => 'required|max_length[100]'],
      // 'responsable' => ['label' => 'Responsable', 'rules' => 'numeric|permit_empty'],
      'cost_price' => ['label' => 'Cost price', 'rules' => 'required'],
      'selling_price' => ['label' => 'Selling price', 'rules' => 'required'],
      // 'evaporation_percent' => ['label' => 'Evaporation percent', 'rules' => 'required'],
      // 'updated_at' => ['label' => 'Updated at', 'rules' => 'permit_empty'],
      // 'created_at' => ['label' => 'Created at', 'rules' => 'permit_empty'],

    ]);
    for ($i = 0; $i < $row_limit; $i++) {
      // $products['id'] = $this->request->getPost('id[' . $i . ']');
      $products['_name'] = $this->request->getPost('name[' . $i . ']');
      $products['cost_price'] = $this->request->getPost('cost_price[' . $i . ']');
      $products['selling_price'] = $this->request->getPost('selling_price[' . $i . ']');


      if ($this->validation->run($products) == FALSE) {
        $response['success'] = false;
        $response['messages'] = $this->validation->listErrors();
        print_r($response['messages']);
        return false;
      } else {
        if ($this->productModel->insert($products)) {
          $response['success'] = true;
          $response['messages'] = 'Data has been inserted successfully';
          // return redirect()->to(base_url(["products", "index"]));
        } else {
          $response['success'] = false;
          $response['messages'] = 'Insertion error!';
          // return false;
        }
      }
    }
    return redirect()->to(base_url(["products", "index"]));


    // $importation = new ImportationModel();

    // $this->validation->setRules([
    //   'filename' => ['label' => 'Filename', 'rules' => 'required|max_length[100]|is_unique[importations.filename]'],
    //   'station_id' => ['label' => 'Station id', 'rules' => 'required|max_length[100]']
    // ]);


    // $fields['filename'] = $file_name;

    // if ($this->validation->run($fields) == FALSE) {
    //   $response['success'] = false;
    //   $response['messages'] = $this->validation->listErrors();
    //   return false;
    // } else {
    //   // insertion dans historique
    //   if ($importation->insert($fields)) {
    //     $response['success'] = true;
    //     $response['messages'] = 'Data has been inserted successfully';
    //     return redirect()->to(base_url(["home", "index"]));
    //   } else {
    //     $response['success'] = false;
    //     $response['messages'] = 'Insertion error!';
    //     echo "error";
    //   }
    // }
  }
}
