<?php

use \Config\Database;

function allProduct()
{
  $db      = Database::connect();
  $builder = $db->table('products');
  $builder->select('id, _name, cost_price, selling_price');
  $query = $builder->get();
  $db->close();
  return $query->getResultArray();
}